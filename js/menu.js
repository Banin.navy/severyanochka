$(document).ready(function () {
  $(".nav__btn").click(function () {
    $(".nav__btn").toggleClass("rotate");
    $(".nav__burger").toggleClass("rotate");
    $(".menu").toggleClass("open__menu");
    $("body").toggleClass("fixed-page");
  });
});
