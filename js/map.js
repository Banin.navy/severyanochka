ymaps.ready(init);
function init() {
  ymaps.ready(init);
  function init() {
    var myMap = new ymaps.Map("map", {
      controls: ["geolocationControl", "zoomControl"],
      center: [61.668796, 50.8365],
      zoom: 15,
      behaviors: ["drag"],
    });
    var myPlacemark = new ymaps.Placemark(
      [61.668796, 50.8365],
      {},
      {
        iconLayout: "default#image",
        iconImageHref: "image/location-pin.png",
        iconImageSize: [70, 70],
        iconImageOffset: [20, 20],
      },
    );
    myMap.geoObjects.add(myPlacemark);
  }
}
